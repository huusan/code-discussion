# Welcome to the code discussion!
This is a simple guessing game where the objective is to find out what number the computer is thinking about. This little challenge focuses on starting discussions around code quality, testability, refactoring etc. rather than being a complex programming test.

## Objective
1. Clone the repository and select a project relevant to your skills (Java or TypeScript + React).
2. Look through the code, change the code, take notes.
3. Walk us through the code and suggest improvements.

__Suggested areas of discussion__

- How would you implement multiplayer?
- Time vs. least attempts?
- Refactor or rewrite?
- How would you implement the game in another language? What language and why?
- How would you improve the UI?
- What do you think about the test coverage?
- How can you make the game harder or easier to win?

## Instructions for Java

__Requirements__

A working installation of [Java JDK](https://openjdk.java.net/) with [Gradle](https://gradle.org/).

__Setup__

1. Clone the repo to your local drive (the folder you will be working with is `java`).
2. Import as Gradle project using [IntelliJ](https://www.jetbrains.com/idea/) or [Eclipse](https://www.eclipse.org/). Or just use your favorite editor.
3. For IntelliJ: Install the Plant-UML and Graphviz plugin to view the UML diagram. Intellij will prompt you to install the plugin. Use a package manager like [APT](https://en.wikipedia.org/wiki/APT_%28software%29) or [DNF](https://en.wikipedia.org/wiki/DNF_%28software%29) to install Graphviz.
4. For Eclipse: Install Eclipse buildship for Gradle support.

__The code__

The program entry point is `guessinggame.Main` in `java/src/main/java/guessinggame/`

__Run the code__

1. Run Guessing game from gradle task: `application -> run`.
2. Run tests from gradle task: `verification -> test`.

## Instructions for TypeScript + React

__Requirements__

A working installation of [Node.js](https://nodejs.org/) with npm.

__Setup__

1. Clone the repo to your local drive (the folder you will be working with is `ts-react`).

__The code__

The program entry point is `main.tsx` in `ts-react/src/`

__Run the code__

1. `npm install`.
2. `npm run build`.
3. Open `index.html` in your browser.

## Instructions for TypeScript + Vue

__Requirements__

A working installation of [Node.js](https://nodejs.org/) version >= 12.0.0 with npm.

__Setup__

1. Clone the repo to your local drive (the folder you will be working with is `vue`).

__The code__

The program entry point is `main.ts` in `vue/src/`

__Run the code__

1. `npm install`.
2. `npm run dev`.
3. Browse to the URL provided by the npm script. (e.g. [http://localhost:3000/](http://localhost:3000/))

## Maintainers
clifford.carnmo@unifaun.com
