import React from 'react';

interface Props {
    scores: { name: string, score: number }[];
}

export default function Leaderboard({ scores }: Props) {
    return (
        <div className="mt-4">
            <h2>Leaderboard</h2>
            <ol>
            { scores.sort((a, b) => a.score - b.score).map(({ name, score }) => (
                    <li>{name}: {score}</li>
            )) }
            </ol>
        </div>
    );
}
